﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calcul_d_aire
{
    public class Program
    {
        public static float Calcul_Aire_Carre(float longueur_cote)
        {
            return longueur_cote * longueur_cote;
        }                      
        public static float Calcul_Aire_Rectangle(float longueur_rectangle,float largeur_rectangle)
        {
            return longueur_rectangle * largeur_rectangle;
        }
        public static float Calcul_Aire_Triangle(float longueur_base,float hauteur_triangle)
        {
            return (longueur_base * hauteur_triangle) / 2;

        }
        static void Main(string[] args)
        {
        }
    }
}
