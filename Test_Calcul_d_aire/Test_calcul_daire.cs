﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calcul_d_aire;

namespace Test_Calcul_d_aire
{
    [TestClass]
    public class Test_calcul_daire
    {
        [TestMethod]
        public void Test_Calcul_Aire_Carre()
        {
            Assert.AreEqual(4,Program.Calcul_Aire_Carre(2));
            Assert.AreEqual(9,Program.Calcul_Aire_Carre(3));
            Assert.AreEqual(1, Program.Calcul_Aire_Carre(1));
            Assert.AreNotEqual(10, Program.Calcul_Aire_Carre(4));            
        }
        [TestMethod]
        public void Test_Calcul_Aire_Rectangle()
        {
            Assert.AreEqual(6, Program.Calcul_Aire_Rectangle(2, 3));
            Assert.AreEqual(9, Program.Calcul_Aire_Rectangle(3, 3));
            Assert.AreEqual(50, Program.Calcul_Aire_Rectangle(5, 10));
            Assert.AreNotEqual(20, Program.Calcul_Aire_Rectangle(6, 6));            
        }
        [TestMethod]
        public void Test_Calcul_Aire_Triangle()
        {
            Assert.AreEqual(15, Program.Calcul_Aire_Triangle(5, 6));
            Assert.AreEqual(8, Program.Calcul_Aire_Triangle(4, 4));
            Assert.AreNotEqual(20, Program.Calcul_Aire_Triangle(5, 10));
        }
    }
}
